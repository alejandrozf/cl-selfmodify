;; Self modifying program

;; 1) Load last version of itself if exists or this code
;; 2) Get data from Internet (or from the machine)
;; 3) Modify (overwrite?) and run his own code according the data obtained in step 2
;; 4) Save this version of the code to a new lisp file as the last version
;; 5) Repeat forever steps 2 to 4

(defmacro selfmodify (&body body)
  `(progn
     (setf *code* ',body)
     (progn ,@body)))

(selfmodify

  (defmacro selfmodify (&body body)
  `(progn
     (setf *code* ',body)
     (progn ,@body)))

  (unless (boundp '*code*)
    (defparameter *code* '(progn)))

  (unless (boundp '*last-version-file*)
    (defparameter *last-version-file* nil))

  (defun live ()
    ;; Start
    (if *last-version-file*
        (with-open-file (s *last-version-file*)
          (setf *code* (read s))))
    ;; Mutate
    (nconc *code* `((print ,(get-universal-time))))
    ;; Live
    (dolist (form *code*)
      (eval form))
    ;; Reproduce
    (let* ((current-date-time (write-to-string (get-universal-time)))
           (last-version-file current-date-time))
      (with-open-file (s last-version-file
                         :direction :output :if-exists :supersede)
        (print *code* s))
      (setf *last-version-file* last-version-file))))



;; TODO: test this scheme
#+nil(loop :while t
           :do (live))
